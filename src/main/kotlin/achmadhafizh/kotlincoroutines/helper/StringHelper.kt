package achmadhafizh.kotlincoroutines.helper

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 22/08/2023, Tuesday
 **/
private val snakeRegex = "_[a-zA-Z]".toRegex()

fun snakeToLowerCamelCase(string: String): String {
    return snakeRegex.replace(string) {
        it.value.replace("_", "").uppercase()
    }
}