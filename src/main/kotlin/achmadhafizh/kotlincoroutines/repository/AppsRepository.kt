package achmadhafizh.kotlincoroutines.repository

import achmadhafizh.kotlincoroutines.model.Apps
import kotlinx.coroutines.flow.Flow

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 22/08/2023, Tuesday
 **/
interface AppsRepository {
    fun findAll(): Flow<Apps.Model>
    fun findCustom(): Flow<MutableMap<String, Any?>>
    suspend fun findById(id: Int): Apps.Model?
    suspend fun create(data: Apps.ModelCreateOrUpdate)
    suspend fun update(id: Int, data: Apps.ModelCreateOrUpdate)
    suspend fun updateDescriptionById(id: Int, data: Apps.ModelUpdateDescription)
    suspend fun deleteById(id: Int)
}