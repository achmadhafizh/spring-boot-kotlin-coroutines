package achmadhafizh.kotlincoroutines.repository

import achmadhafizh.kotlincoroutines.mapper.AppsRowMapper
import achmadhafizh.kotlincoroutines.mapper.DynamicRowMapper
import achmadhafizh.kotlincoroutines.model.Apps
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.reactive.awaitFirstOrNull
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.r2dbc.core.DatabaseClient
import org.springframework.r2dbc.core.await
import org.springframework.r2dbc.core.flow
import org.springframework.stereotype.Repository

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 22/08/2023, Tuesday
 **/
@Repository
class AppsRepositoryImpl: AppsRepository {
    @Autowired
    private lateinit var databaseClient: DatabaseClient

    override fun findAll(): Flow<Apps.Model> {
        return databaseClient
            .sql("SELECT * FROM apps")
            .map(AppsRowMapper())
            .flow()
    }

    override fun findCustom(): Flow<MutableMap<String, Any?>> {
        return databaseClient
            .sql("SELECT * FROM apps")
            .map(DynamicRowMapper())
            .flow()
    }

    override suspend fun findById(id: Int): Apps.Model? {
        return databaseClient
            .sql("SELECT * FROM apps WHERE id = :id")
            .bind("id", id)
            .map(AppsRowMapper())
            .one()
            .awaitFirstOrNull()
    }

    override suspend fun create(data: Apps.ModelCreateOrUpdate) {
        databaseClient
            .sql("INSERT INTO apps (name, description) VALUES (:name, :description)")
            .bind("name", data.name)
            .bind("description", data.description)
            .await()
    }

    override suspend fun update(id: Int, data: Apps.ModelCreateOrUpdate) {
        databaseClient
            .sql("UPDATE apps SET name = :name, description = :description WHERE id = :id")
            .bind("name", data.name)
            .bind("description", data.description)
            .bind("id", id)
            .await()
    }

    override suspend fun updateDescriptionById(id: Int, data: Apps.ModelUpdateDescription) {
        return databaseClient
            .sql("UPDATE apps SET description = :description WHERE id = :id")
            .bind("description", data.description)
            .bind("id", id)
            .await()
    }

    override suspend fun deleteById(id: Int) {
        return databaseClient
            .sql("DELETE FROM apps WHERE id = :id")
            .bind("id", id)
            .await()
    }
}