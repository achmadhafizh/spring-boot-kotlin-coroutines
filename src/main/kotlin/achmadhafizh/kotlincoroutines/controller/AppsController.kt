package achmadhafizh.kotlincoroutines.controller

import achmadhafizh.kotlincoroutines.constant.ResponseConstant.RECORDS_CREATED
import achmadhafizh.kotlincoroutines.constant.ResponseConstant.RECORDS_DELETED
import achmadhafizh.kotlincoroutines.constant.ResponseConstant.RECORDS_FOUND
import achmadhafizh.kotlincoroutines.constant.ResponseConstant.RECORDS_NOT_FOUND
import achmadhafizh.kotlincoroutines.constant.ResponseConstant.RECORDS_UPDATED
import achmadhafizh.kotlincoroutines.model.ApiResponse
import achmadhafizh.kotlincoroutines.model.Apps
import achmadhafizh.kotlincoroutines.service.AppsService
import kotlinx.coroutines.flow.toList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 22/08/2023, Tuesday
 **/
@RestController
@RequestMapping("/apps")
class AppsController {
    @Autowired
    private lateinit var appsService: AppsService

    @GetMapping
    suspend fun getAll(): ResponseEntity<ApiResponse> {
        val result = appsService.findAll().toList()
        val response = ApiResponse()

        if (result.isNotEmpty()) {
            response.message = RECORDS_FOUND
            response.totalData = result.size
            response.data = result
        } else {
            response.message = RECORDS_NOT_FOUND
        }

        return ResponseEntity(response, HttpStatus.OK)
    }

    @GetMapping("/custom")
    suspend fun getCustom(): ResponseEntity<ApiResponse> {
        val result = appsService.findCustom().toList()
        val response = ApiResponse()

        if (result.isNotEmpty()) {
            response.message = RECORDS_FOUND
            response.totalData = result.size
            response.data = result
        } else {
            response.message = RECORDS_NOT_FOUND
        }

        return ResponseEntity(response, HttpStatus.OK)
    }

    @GetMapping("/{id}")
    suspend fun getById(@PathVariable id: Int): ResponseEntity<ApiResponse> {
        val result = appsService.findById(id)
        val response = ApiResponse()

        if (result != null) {
            response.message = RECORDS_FOUND
            response.totalData = 1
            response.data = result
        } else {
            response.message = RECORDS_NOT_FOUND
        }

        return ResponseEntity(response, HttpStatus.OK)
    }

    @PostMapping
    suspend fun create(@RequestBody data: Apps.ModelCreateOrUpdate): ResponseEntity<ApiResponse> {
        appsService.save(data)
        return ResponseEntity(ApiResponse(RECORDS_CREATED), HttpStatus.CREATED)
    }

    @PutMapping("/{id}")
    suspend fun update(
        @PathVariable id: Int,
        @RequestBody data: Apps.ModelCreateOrUpdate
    ): ResponseEntity<ApiResponse> {
        appsService.save(data, id)
        return ResponseEntity(ApiResponse(RECORDS_UPDATED), HttpStatus.OK)
    }

    @PutMapping("/description/{id}")
    suspend fun updateDescriptionById(
        @PathVariable id: Int,
        @RequestBody data: Apps.ModelUpdateDescription
    ): ResponseEntity<ApiResponse> {
        appsService.updateDescriptionById(id, data)
        return ResponseEntity(ApiResponse(RECORDS_UPDATED), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    suspend fun deleteById(@PathVariable id: Int): ResponseEntity<ApiResponse> {
        appsService.deleteById(id)
        return ResponseEntity(ApiResponse(RECORDS_DELETED), HttpStatus.OK)
    }
}