package achmadhafizh.kotlincoroutines.service

import achmadhafizh.kotlincoroutines.model.Apps
import achmadhafizh.kotlincoroutines.repository.AppsRepository
import kotlinx.coroutines.flow.Flow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 22/08/2023, Tuesday
 **/
@Service
class AppsService {
    @Autowired
    private lateinit var appsRepository: AppsRepository

    fun findAll(): Flow<Apps.Model> {
        return appsRepository.findAll()
    }

    fun findCustom(): Flow<MutableMap<String, Any?>> {
        return appsRepository.findCustom()
    }

    suspend fun findById(id: Int): Apps.Model? {
        return appsRepository.findById(id)
    }

    suspend fun save(data: Apps.ModelCreateOrUpdate, id: Int? = null) {
        if(id != null) appsRepository.update(id, data) else appsRepository.create(data)
    }

    suspend fun updateDescriptionById(id: Int, data: Apps.ModelUpdateDescription) {
        appsRepository.updateDescriptionById(id, data)
    }

    suspend fun deleteById(id: Int) {
        appsRepository.deleteById(id)
    }
}