package achmadhafizh.kotlincoroutines.constant

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 22/08/2023, Tuesday
 **/
object ResponseConstant {
    const val RECORDS_FOUND = "Record found."
    const val RECORDS_NOT_FOUND = "No record found!"
    const val RECORDS_CREATED = "Record has been created."
    const val RECORDS_UPDATED = "Record has been updated."
    const val RECORDS_DELETED = "Record has been deleted."
}