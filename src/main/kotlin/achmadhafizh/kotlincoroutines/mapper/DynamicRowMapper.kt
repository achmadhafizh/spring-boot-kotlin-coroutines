package achmadhafizh.kotlincoroutines.mapper

import achmadhafizh.kotlincoroutines.helper.snakeToLowerCamelCase
import io.r2dbc.spi.Row
import io.r2dbc.spi.RowMetadata
import java.util.function.BiFunction

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 22/08/2023, Tuesday
 **/
class DynamicRowMapper: BiFunction<Row, RowMetadata, MutableMap<String, Any?>> {
    override fun apply(row: Row, rowMetadata: RowMetadata): MutableMap<String, Any?> {
        val totalColumns = rowMetadata.columnMetadatas.size
        var index = 0
        val objectMapping = mutableMapOf<String, Any?>()
        while (index < totalColumns) {
            objectMapping[snakeToLowerCamelCase(rowMetadata.getColumnMetadata(index).name)] = row[index]
            index += 1
        }
        return objectMapping
    }
}