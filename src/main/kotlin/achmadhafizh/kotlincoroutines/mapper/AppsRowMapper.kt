package achmadhafizh.kotlincoroutines.mapper

import achmadhafizh.kotlincoroutines.model.Apps
import io.r2dbc.spi.Row
import io.r2dbc.spi.RowMetadata
import java.util.function.BiFunction

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 22/08/2023, Tuesday
 **/
class AppsRowMapper: BiFunction<Row, RowMetadata, Apps.Model> {
    override fun apply(row: Row, rowMetadata: RowMetadata): Apps.Model {
        val totalColumns = rowMetadata.columnMetadatas.size
        var index = 0
        val objectMapping = Apps.Model()
        while (index < totalColumns) {
            objectMapping.id = row["id"].toString().toInt()
            objectMapping.name = row["name"].toString()
            objectMapping.description = row["description"].toString()
            index += 1
        }
        return objectMapping
    }
}