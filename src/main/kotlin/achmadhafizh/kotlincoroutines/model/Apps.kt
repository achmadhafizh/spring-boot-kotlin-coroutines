package achmadhafizh.kotlincoroutines.model

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 22/08/2023, Tuesday
 **/
object Apps {
    data class Model(var id: Int, var name: String, var description: String) {
        constructor(): this(0, "", "")
    }
    data class ModelCreateOrUpdate(val name: String, val description: String)
    data class ModelUpdateDescription(val description: String)
}