package achmadhafizh.kotlincoroutines.model

import java.text.SimpleDateFormat
import java.util.*

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 22/08/2023, Tuesday
 **/
data class ApiResponse(
    var message: String,
    var totalData: Int = 0,
    var data: Any? = null,
    val timestamp: String = SimpleDateFormat("yyyy-MM-dd HH:mm:ss z").format(Date())
) {
    constructor(): this("")
}