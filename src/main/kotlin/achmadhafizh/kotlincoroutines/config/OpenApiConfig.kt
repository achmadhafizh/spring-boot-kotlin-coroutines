package achmadhafizh.kotlincoroutines.config

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.info.License
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.info.BuildProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 22/08/2023, Tuesday
 **/
@Configuration
class OpenApiConfig {
    @Autowired
    private lateinit var buildProperties: BuildProperties

    @Bean
    fun springOpenAPI(): OpenAPI? {
        val contact: Contact? = null
        contact?.name = "Achmad Hafizh"
        contact?.email = "achmadhafizhh@gmail.com"
        contact?.url = "https://www.linkedin.com/in/achmad-hafizh-296361148/"

        return OpenAPI()
            .info(
                Info().title("Restful API")
                    .description("This is Restful API using Kotlin Coroutines")
                    .version(buildProperties.version)
                    .license(License().name("Apache 2.0").url("http://springdoc.org"))
                    .contact(contact)
            )
    }
}