package achmadhafizh.kotlincoroutines.config

import io.r2dbc.pool.PoolingConnectionFactoryProvider.*
import io.r2dbc.spi.ConnectionFactories
import io.r2dbc.spi.ConnectionFactory
import io.r2dbc.spi.ConnectionFactoryOptions
import io.r2dbc.spi.ConnectionFactoryOptions.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.r2dbc.connection.R2dbcTransactionManager
import org.springframework.r2dbc.connection.TransactionAwareConnectionFactoryProxy
import org.springframework.r2dbc.connection.init.CompositeDatabasePopulator
import org.springframework.r2dbc.connection.init.ConnectionFactoryInitializer
import org.springframework.r2dbc.connection.init.ResourceDatabasePopulator
import org.springframework.r2dbc.core.DatabaseClient
import org.springframework.transaction.ReactiveTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.transaction.reactive.TransactionalOperator
import java.time.Duration

/**
 * @author : Achmad Hafizh
 * @mailto : achmadhafizhh@gmail.com
 * @created : 22/08/2023, Tuesday
 **/
@Configuration
@EnableTransactionManagement
class DatabaseConfig {
    @Value("\${r2dbc.driver}")
    private lateinit var driver: String

    @Value("\${r2dbc.host}")
    private lateinit var host: String

    @Value("\${r2dbc.port}")
    private lateinit var port: String

    @Value("\${r2dbc.database}")
    private lateinit var database: String

    @Value("\${r2dbc.username}")
    private lateinit var user: String

    @Value("\${r2dbc.password}")
    private lateinit var password: String

    @Value("\${r2dbc.connect-timeout}")
    private lateinit var connectTimeout: String

    @Value("\${r2dbc.pool.initial-size}")
    private lateinit var initialSize: String

    @Value("\${r2dbc.pool.max-size}")
    private lateinit var maxSize: String

    @Value("\${r2dbc.pool.max-idle-time}")
    private lateinit var maxIdleTime: String

    @Value("\${r2dbc.pool.validation-query}")
    private lateinit var validationQuery: String

    @Bean
    fun connectionFactory(): ConnectionFactory {
        val options = ConnectionFactoryOptions.builder()
            .option(DRIVER, driver)
            .option(HOST, host)
            .option(USER, user)
            .option(PORT, port.toInt()) // optional, default 3306
            .option(PASSWORD, password) // optional, default null, null means has no password
            .option(DATABASE, database) // optional, default null, null means not specifying the database
            .option(INITIAL_SIZE, initialSize.toInt()) // Initial pool size. Defaults to 10.
            .option(MAX_SIZE, maxSize.toInt()) // Maximum pool size. Defaults to 10.
            .option(MAX_IDLE_TIME, Duration.ofMinutes(maxIdleTime.toLong())) // Maximum idle time of the connection in the pool.
            .option(VALIDATION_QUERY, validationQuery) // Query that will be executed just before a connection is given to you from the pool to validate that the connection to the database is still alive.
            .option(CONNECT_TIMEOUT, Duration.ofSeconds(connectTimeout.toLong())) // optional, default null, null means no timeout
            .build()

        return ConnectionFactories.get(options)
    }

    @Bean
    fun transactionAwareConnectionFactoryProxy(connectionFactory: ConnectionFactory): TransactionAwareConnectionFactoryProxy {
        return TransactionAwareConnectionFactoryProxy(connectionFactory)
    }

    @Bean
    fun databaseClient(connectionFactory: ConnectionFactory): DatabaseClient {
        return DatabaseClient.builder()
            .connectionFactory(connectionFactory)
            .namedParameters(true)
            .build()
    }

    @Bean
    fun transactionManager(connectionFactory: ConnectionFactory): ReactiveTransactionManager {
        return R2dbcTransactionManager(connectionFactory)
    }

    @Bean
    fun transactionalOperator(transactionManager: ReactiveTransactionManager): TransactionalOperator {
        return TransactionalOperator.create(transactionManager)
    }

    @Bean
    fun initializer(connectionFactory: ConnectionFactory): ConnectionFactoryInitializer {
        val initializer = ConnectionFactoryInitializer()
        initializer.setConnectionFactory(connectionFactory)
        val populator = CompositeDatabasePopulator()
        populator.addPopulators(ResourceDatabasePopulator(ClassPathResource("migration/scheme/apps.sql")))
        populator.addPopulators(ResourceDatabasePopulator(ClassPathResource("migration/seeder/apps-data.sql")))
        initializer.setDatabasePopulator(populator)
        return initializer
    }
}